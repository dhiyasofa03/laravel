<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/register', [App\Http\Controllers\AuthController::class, 'register']);
Route::get('/welcome/{first_name}/{last_name}', [App\Http\Controllers\AuthController::class, 'welcome'])->name('welcome');
Route::post('/send-name', [App\Http\Controllers\AuthController::class, 'sendName']);