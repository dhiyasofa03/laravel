<!DOCTYPE html>
<html>
    <head>
        <title>signup</title>
    </head>
    <body>

        <h1>Buat Account Baru!</h1>
        <h4>Sign Up Form</h4>
        <form method="POST" action="/send-name">
          @csrf
            <label>First name:</label><br><br>
            <input type="text" name="first_name"><br><br>
            <label>Last name:</label><br><br>
            <input type="text" name="last_name"><br><br>
            <label>Gender</label><br><br>
            <input type="radio" id="male"><label for="male">Male</label><br>
            <input type="radio" id="female"><label for="female">Female</label><br>
            <input type="radio" id="other"><label for="other">Other</label><br><br>
            <label>Nationality</label><br><br>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>
            </select><br><br>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" id="bahasa_indonesia"><label for="bahasa_indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" id="english"><label for="english">English</label><br>
            <input type="checkbox" id="other"><label for="other">Other</label><br><br>
            <label>Bio:</label><br><br>
            <textarea></textarea><br>
            <input type="submit" value="Sign Up">
        </form>
        
    </body>
</html>