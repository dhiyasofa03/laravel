<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function sendName(Request $request)
    {
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        return redirect()->route('welcome', [
            'first_name' => $first_name,
            'last_name' => $last_name,
        ]);
    }

    public function welcome($first_name, $last_name)
    {
        return view('welcome' , [
            'first_name' => $first_name,
            'last_name' => $last_name,
        ]);
    }
}
